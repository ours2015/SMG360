//
//  AppDelegate.h
//  SMG360
//
//  Created by 段大志 on 16/6/27.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(assign, nonatomic) BOOL allowRotation;//运行屏幕选择


+ (AppDelegate *)appDelegate;

@end

