//
//  FirstViewController.m
//  SMG360
//
//  Created by 段大志 on 16/7/21.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import "FirstViewController.h"
#import "LocalViewController.h"
#import "HTYGLKVC.h"
#import "HTYMenuVC.h"
#import "QMUILabel.h"
#import "LocalScollViewController.h"

@interface FirstViewController ()
@property (weak, nonatomic) IBOutlet QMUILabel *scrollLable;
@property (weak, nonatomic) IBOutlet QMUILabel *localLb;
@property (weak, nonatomic) IBOutlet QMUILabel *netLb;
@property (weak, nonatomic) IBOutlet QMUILabel *fullLb;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.scrollLable.userInteractionEnabled = YES;
    self.localLb.userInteractionEnabled = YES;
    self.netLb.userInteractionEnabled   = YES;
    self.fullLb.userInteractionEnabled  = YES;

    self.scrollLable.verticalAlignment = VerticalAlignmentMiddle;
    self.localLb.verticalAlignment = VerticalAlignmentMiddle;
    self.netLb.verticalAlignment = VerticalAlignmentMiddle;
    self.fullLb.verticalAlignment = VerticalAlignmentMiddle;

    [self.scrollLable addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(localScrollPalyer)]];
    [self.localLb addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(localPalyer)]];
    [self.netLb addGestureRecognizer:[[UITapGestureRecognizer alloc]   initWithTarget:self action:@selector(netPlayer)]];
    [self.fullLb addGestureRecognizer:[[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(funllPlayer)]];
}

/**
 * 可以滚动
 */
- (void) localScrollPalyer {
    LocalScollViewController *localViewController = [[LocalScollViewController alloc] init];
    localViewController.isLocal = YES;
    localViewController.title = @"滚动本地";
    [self.navigationController pushViewController:localViewController animated:YES];
}

/**
 * 本地
 */
- (void) localPalyer {
    LocalViewController *localViewController = [[LocalViewController alloc] init];
    localViewController.isLocal = YES;
    localViewController.title = @"本地";
    [self.navigationController pushViewController:localViewController animated:YES];
}

/**
 * 网络
 */
- (void) netPlayer {
    LocalViewController *localViewController = [[LocalViewController alloc] init];
    localViewController.isLocal = NO;
    localViewController.title = @"网络";
    [self.navigationController pushViewController:localViewController animated:YES];
}

/**
 * 全景
 */
- (void) funllPlayer {
    HTYMenuVC *htyglkvc = [[HTYMenuVC alloc] init];
    htyglkvc.title = @"全景";
    [self.navigationController pushViewController:htyglkvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
