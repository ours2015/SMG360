//
// Created by 段大志 on 15/11/23.
// Copyright (c) 2015 ourslook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CATrasitionType) {
    CATrasitionType_pageCurl,//向上翻一页
    CATrasitionType_pageUnCurl,//向上翻一页
    CATrasitionType_rippleEffect,//滴水效果
    CATrasitionType_suckEffect,// 收缩效果，如一块布被抽走
    CATrasitionType_cube,//立方体效果
    CATrasitionType_oglFlip,//上下翻转效果
} NS_ENUM_AVAILABLE_IOS(7_0);


/**
 * ios动画主要有两种
 * 1:UIView层的动画,frame是真的变化了
 * 2:更低层次的CAAnimal子类-->CATransition->CAGroup-->Base...-->Frame...动画,这个是基于layer,没有真的形变
 * 3:这里全部是私有api,注意上线项目不要使用
 */
@interface CAAnimationHelp : NSObject

#pragma mark CATrasition动画,深层次的,layer层动画
// CATransition 动画效果type http://blog.csdn.net/yuhuangc/article/details/6804056
+ (void)addAnimalUseCATrasition:(UIView *)view withType:(CATrasitionType)type ;
@end