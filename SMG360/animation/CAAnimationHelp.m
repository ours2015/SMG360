//
// Created by 段大志 on 15/11/23.
// Copyright (c) 2015 ourslook. All rights reserved.
//

#import "CAAnimationHelp.h"


@implementation CAAnimationHelp

/**
 * 添加翻页动画
 */
+ (void)addAnimalUseCATrasition:(UIView *)view withType:(CATrasitionType)type
{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.75;
    NSString *typeStr = @"";
    switch (type) {
        case CATrasitionType_pageCurl://向上翻一页
            typeStr = @"pageCurl";
            break;
        case CATrasitionType_pageUnCurl://向上翻一页
            typeStr = @"pageUnCurl";
            break;
        case CATrasitionType_rippleEffect://滴水效果
            typeStr = @"rippleEffect";
            break;
        case CATrasitionType_suckEffect://收缩效果，如一块布被抽走
            typeStr = @"suckEffect";
            break;
        case CATrasitionType_cube://立方体效果
            typeStr = @"cube";
            break;
        case CATrasitionType_oglFlip://上下翻转效果
            typeStr = @"oglFlip";
            break;
    }
    transition.type = typeStr;
    [transition setFillMode:kCAFillModeForwards];
    transition.subtype = kCATransitionFromRight;
    [view.layer addAnimation:transition forKey:nil];
}


@end