//
//  QMUILabel.h
//  smg
//
//  Created by wuguanpin on 15-1-28.
//  Copyright (c) 2015年 diyick. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

//支持垂直方向设置的UILable
@interface QMUILabel : UILabel{
    @private VerticalAlignment _verticalAlignment;
}

@property (nonatomic) VerticalAlignment verticalAlignment;

@end
