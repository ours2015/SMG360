//
//  UIView+Extension.h
//  ZiChanBao
//
//  Created by liujinliang on 15/6/11.
//  Copyright (c) 2015年 WorldUnion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

#pragma mark - 坐标属性
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat bottom;

#pragma mark - 创建实例
/**
 *  创建指定长度的横线，0.5px，用作分隔线
 *
 *  @param length 横线的长度
 */
+ (instancetype)horizontalLineWithLength:(CGFloat)length;

/**
 *  创建指定长度的横线，0.5px，用作分隔线
 *
 *  @param length 横线的长度
 *  @param color  颜色
 */
+ (instancetype)horizontalLineWithLength:(CGFloat)length color:(UIColor *)color;

/**
 *  创建指定长度的竖线，0.5px，用作分隔线
 *
 *  @param length 竖线的长度
 */
+ (instancetype)verticalLineWithLength:(CGFloat)length;

/**
 *  创建指定长度的竖线，0.5px，用作分隔线
 *
 *  @param length 竖线的长度
 *  @param color  颜色
 */
+ (instancetype)verticalLineWithLength:(CGFloat)length color:(UIColor *)color;

#pragma mark - 设置Layer属性
/**
 *  设置圆角
 */
- (void)setLayerCornerRadius:(CGFloat)radius;
- (void)setLayerCornerRadius:(CGFloat)radius color:(UIColor *)color;
- (void)setLayerCornerRadius:(CGFloat)radius color:(UIColor *)color borderWidth:(CGFloat)borderWidth;

/**
 *  设置圆角
 */
- (void)setLayerBorderWidth:(CGFloat)borderWidth color:(UIColor *)color;

#pragma mark - 调试用
/**
 *  调试用，显示边框
 */
- (void)debugBorder;

/**
 *  调试用，打印frame
 */
- (void)debugFrame;

/**
 *  移除所有的ziview
 */
- (void) removeAllSubViews;

@end
