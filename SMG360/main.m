//
//  main.m
//  SMG360
//
//  Created by 段大志 on 16/6/27.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
