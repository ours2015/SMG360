//
//  test.m
//  SMG360
//
//  Created by 吉源 on 16/7/22.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import "test.h"

@interface test ()
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation test

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *imag = [UIImage imageNamed:@"147E4217-E1B7-45B0-9DAE-41744CD2B0EF"];
    [_button setBackgroundImage:imag forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)action:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
