//
// Created by 段大志 on 8/23/16.
// Copyright (c) 2016 DanielDuan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AVPlayerItem;
@class AVQueuePlayer;
@class AVPlayerLayer;


@interface AVPlayerSingle : NSObject


@property (nonatomic, strong, readonly) NSMutableDictionary *dictionary;
@property (assign, nonatomic, readonly) int current;

+ (AVPlayerSingle *)instance;

- (void) initAllVideo:(NSMutableArray<AVPlayerItem*> *)avplayerItems;
- (void) addToParentView:(UIView *)parentView;
- (void) playerItem:(int)index;

- (void) pause;
- (void) destory;

@end