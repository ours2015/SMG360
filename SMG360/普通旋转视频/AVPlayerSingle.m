//
// Created by 段大志 on 8/23/16.
// Copyright (c) 2016 DanielDuan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AVPlayerSingle.h"

//avplayer+缓存:http://www.jianshu.com/p/93ce1748ea57

@interface AVPlayerSingle () {
}
@property(nonatomic, strong) AVQueuePlayer *queuePlayer;
@property(nonatomic, strong) AVPlayerLayer *layer;
@property(nonatomic, strong) NSMutableArray<AVPlayerItem *> *playerItems;
@property(nonatomic, weak) UIView *parentView;
@property(nonatomic, assign) Float64 currentTimeSeconds;


@property(nonatomic, strong) AVPlayerItemVideoOutput *videoOutput;

@end

@implementation AVPlayerSingle

+ (AVPlayerSingle *)instance {
    static AVPlayerSingle *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];

            [_instance initialized];
        }
    }
    return _instance;
}


- (void)initialized {
    //数据初始化
    _dictionary = [NSMutableDictionary dictionary];
    _videoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:nil];
}

- (void)initAllVideo:(NSMutableArray<AVPlayerItem *> *)avplayerItems {
    [self.queuePlayer removeAllItems];
    self.playerItems = avplayerItems;

    self.queuePlayer = [AVQueuePlayer queuePlayerWithItems:avplayerItems];
    self.layer = [AVPlayerLayer playerLayerWithPlayer:self.queuePlayer];
    self.layer.backgroundColor = [UIColor grayColor].CGColor;
    self.layer.frame = CGRectZero;

    [self setTheProgressOfPlayTime];
}


- (void)addToParentView:(UIView *)parentView {
    if (self.parentView) {
        [self.layer removeFromSuperlayer];
    }
    self.parentView = parentView;
    self.layer.frame = self.parentView.frame;
    [self.parentView.layer addSublayer:self.layer];
    [self.queuePlayer play];
}

- (void)playerItem:(int)index {

    if (index == self.current) {
        return;
    }


    CMTime itemTime = [_videoOutput itemTimeForHostTime:CACurrentMediaTime()];
    if ([_videoOutput hasNewPixelBufferForItemTime:itemTime]) {
        CVPixelBufferRef pixelBuffer = [_videoOutput copyPixelBufferForItemTime:itemTime itemTimeForDisplay:nil];
        // 利用 CIFilter 处理 CIImage
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithCIImage:[CIImage imageWithCVPixelBuffer:pixelBuffer]]];
        imageView.frame = self.layer.videoRect;
        if (imageView) {
            [self.dictionary setObject:imageView forKey:@(index)];
        }
        CVPixelBufferRelease(pixelBuffer);
    }


    _current = index;
    int32_t timeScale = self.queuePlayer.currentItem.asset.duration.timescale;
    [self.queuePlayer removeAllItems];
    for (int i = index; i < self.playerItems.count; i++) {
        AVPlayerItem *obj = [self.playerItems objectAtIndex:i];
        if ([self.queuePlayer canInsertItem:obj afterItem:nil]) {

            [self.queuePlayer insertItem:obj afterItem:nil];

            //转换成CMTime才能给player来控制播放进度
            Float64 seconds = self.currentTimeSeconds;
            NSLog(@"--------------%lf", seconds);

            @try {
                [obj seekToTime:CMTimeMakeWithSeconds(fabs(seconds), timeScale)
                toleranceBefore:kCMTimeZero
                 toleranceAfter:kCMTimeZero
              completionHandler:^(BOOL finished) {
                  [self.queuePlayer play];
              }];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception occurred: %@, %@", exception, [exception userInfo]);
            }

            break;
        }
    }


    [self.playerItems enumerateObjectsUsingBlock:^(AVPlayerItem *obj, NSUInteger idx, BOOL *stop) {
        [obj removeOutput:_videoOutput];
    }];

    [self.queuePlayer.currentItem addOutput:_videoOutput];

}

- (void)pause {
    [self.queuePlayer pause];
}

- (void)destory {
    [self.queuePlayer pause];
    [self.queuePlayer.items enumerateObjectsUsingBlock:^(AVPlayerItem *obj, NSUInteger idx, BOOL *stop) {
        [obj removeOutput:_videoOutput];
    }];
    self.queuePlayer = nil;
    _current = 0;
    self.currentTimeSeconds = 0;
    self.playerItems = nil;
}


//设置播放进度和时间
- (void)setTheProgressOfPlayTime {

    [self.queuePlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 100) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {

        Float64 current = CMTimeGetSeconds(time);
        self.currentTimeSeconds = current;

        NSLog(@"%lf", current);

//        NSLog(@"%lf", current);

//        float total=CMTimeGetSeconds([playerItem duration]);

//        //秒数
//        NSInteger proSec = (NSInteger) current % 60;
//        //分钟
//        NSInteger proMin = (NSInteger) current / 60;
//
//        //总秒数和分钟
//        NSInteger durSec = (NSInteger) total % 60;
//        NSInteger durMin = (NSInteger) total / 60;
//        NSString *totalStr = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];

//        NSLog(@"当前已经播放%.2fs;  总秒数:%li; 帧率:%i;  总S:%@",  current, current/total,  timescale, totalStr);
    }];
}


@end