//
//  JSCarouselViewModel.m
//  JSCarouselDemo
//
//  Created by 乔同新 on 16/6/13.
//  Copyright © 2016年 乔同新. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "JSCarouselViewModel.h"
#import "JSGoodsModel.h"
#import "AVPlayerSingle.h"

@implementation JSCarouselViewModel

- (void)getData{


    AVPlayerSingle * avPlayerSingle = [AVPlayerSingle instance];

    NSMutableArray<AVPlayerItem*> *avplayerItems = [NSMutableArray array];
    NSInteger count = 3;

    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:count];
    int frakeIndex = 0;
    for (int i = 0; i<count; i++) {
        JSGoodsModel *model = [[JSGoodsModel alloc] init];
        model.current = i;

        NSString *path  = [[NSBundle mainBundle]  pathForResource:[NSString stringWithFormat:@"aa%i",1+i] ofType:@"avi"];
        NSURL *url  =[NSURL fileURLWithPath:path];
        AVPlayerItem *thePlayerItemA = [[AVPlayerItem alloc] initWithURL:url];
        [avplayerItems addObject:thePlayerItemA];

        model.p_price = 10.0+i;
        model.p_name = [NSString stringWithFormat:@"%@+++++-",@(i)];
        
        frakeIndex++;
        frakeIndex = frakeIndex>3?0:frakeIndex;
        
        [data addObject:model];
    }
    [avPlayerSingle initAllVideo:avplayerItems];

    self.data = data;
}

@end
