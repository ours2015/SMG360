//
//  JSGoodsModel.h
//  JSCarouselDemo
//
//  Created by 乔同新 on 16/6/13.
//  Copyright © 2016年 乔同新. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AVPlayerItem;

@interface JSGoodsModel : NSObject

@property (nonatomic, assign) int current;
@property (nonatomic, strong) NSString *p_name;

@property (nonatomic, strong) UIImage *p_imageURL;

@property (nonatomic, assign) float     p_price;

@end
