//
//  LocalViewController.h
//  SMG360
//
//  Created by 段大志 on 16/6/27.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import <UIKit/UIKit.h>


#define UIScreenWidth  [UIScreen mainScreen].bounds.size.width
#define UIScreenHeight [UIScreen mainScreen].bounds.size.height

@interface LocalScollViewController : UIViewController
@property (nonatomic, assign) BOOL isLocal;
@end

