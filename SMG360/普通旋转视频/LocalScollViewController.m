//
//  LocalViewController.m
//  SMG360
//
//  Created by 段大志 on 16/6/27.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import "LocalScollViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "JSCarouselLayout.h"
#import "JSCarouselUIService.h"
#import "JSCarouselViewModel.h"
#import "AVPlayerSingle.h"


static NSString *cellid = @"cellBid";
// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, PanDirection) {
    PanDirectionHorizontalMoved, //横向移动
    PanDirectionVerticalMoved    //纵向移动
};

@interface LocalScollViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) UICollectionView *uiCollectionView;
@property (nonatomic, strong) JSCarouselUIService *service;
@property (nonatomic, strong) JSCarouselViewModel *viewModel;

@end

@implementation LocalScollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"首页......");

    //初始化数据
    self.view.backgroundColor = [UIColor whiteColor];
    [AVPlayerSingle instance];
    [self.viewModel getData];
    [self.uiCollectionView reloadData];
    self.uiCollectionView.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self.uiCollectionView reloadData];
    self.uiCollectionView.hidden = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[AVPlayerSingle instance] pause];
}


- (void)dealloc {
    NSLog(@"dealloc--");
    [[AVPlayerSingle instance] destory];
}

- (void)viewWillLayoutSubviews NS_AVAILABLE_IOS(5_0) {
    [super viewWillLayoutSubviews];
    NSLog(@"viewWillLayoutSubviews--");
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    NSLog(@"viewDidLayoutSubviews--");
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    AVPlayerItem *playerItem=object;
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
        if(status==AVPlayerStatusReadyToPlay){
            NSLog(@"正在播放...，视频总长度:%.2f",CMTimeGetSeconds(playerItem.duration));
        }
    }
    else if([keyPath isEqualToString:@"loadedTimeRanges"])
    {
        NSArray *array=playerItem.loadedTimeRanges;
        CMTimeRange timeRange = [array.firstObject CMTimeRangeValue];//本次缓冲时间范围
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        NSTimeInterval totalBuffer = startSeconds + durationSeconds;//缓冲总长度
        NSLog(@"共缓冲：%.2f",totalBuffer);
    }
}


#pragma mark - lazy load
- (JSCarouselViewModel *)viewModel{

    if (!_viewModel) {
        _viewModel = [[JSCarouselViewModel alloc] init];
    }
    return _viewModel;
}

- (JSCarouselUIService *)service{

    if (!_service) {
        _service = [[JSCarouselUIService alloc] init];
        _service.viewModel = self.viewModel;
    }
    return _service;
}

- (UICollectionView *)uiCollectionView {
    if (!_uiCollectionView) {

        JSCarouselLayout *layout = [[JSCarouselLayout alloc] init];
        __weak typeof(self) weakSelf = self;
        layout.carouselSlideIndexBlock          = ^(NSInteger index){
            NSLog(@"%i", index);
            [[AVPlayerSingle instance] playerItem:index];
            [weakSelf.uiCollectionView reloadData];
        };
        layout.itemSize                         = CGSizeMake(UIScreenWidth, UIScreenHeight-100);
        _uiCollectionView = [[UICollectionView alloc] initWithFrame:(CGRect){0,100, UIScreenWidth, UIScreenHeight-100} collectionViewLayout:layout];
        [self.view addSubview:_uiCollectionView];
        _uiCollectionView.dataSource = self.service;
        _uiCollectionView.delegate   = self.service;
        _uiCollectionView.showsHorizontalScrollIndicator = NO;
        _uiCollectionView.showsVerticalScrollIndicator = NO;
        UINib *cellNib=[UINib nibWithNibName:@"JSCarouselGoodsCell" bundle:nil];
        [_uiCollectionView registerNib:cellNib forCellWithReuseIdentifier:@"JSCarouselGoodsCell"];

        [self.viewModel getData];
        [_uiCollectionView reloadData];
    }
    return _uiCollectionView;
}

@end
