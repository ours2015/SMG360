//
//  BCollectionViewCell.h
//  mycode
//
//  Created by LQL on 16/5/27.
//  Copyright © 2016年 LQL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@end
