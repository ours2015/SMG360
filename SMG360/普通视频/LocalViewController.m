//
//  LocalViewController.m
//  SMG360
//
//  Created by 段大志 on 16/6/27.
//  Copyright © 2016年 DanielDuan. All rights reserved.
//

#import "LocalViewController.h"
#import "QMAVplayerVC.h"
#import "XSMediaPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "HYBCardScaleFlowLayout.h"
#import "BCollectionViewCell.h"
#import "JSCarouselLayout.h"
#import "JSCarouselUIService.h"
#import "JSCarouselViewModel.h"
#import "CAAnimationHelp.h"

typedef enum : NSInteger {
    kCameraMoveDirectionNone,
    kCameraMoveDirectionUp,
    kCameraMoveDirectionDown,
    kCameraMoveDirectionRight,
    kCameraMoveDirectionLeft
} CameraMoveDirection;
CGFloat const gestureMinimumTranslation = 10.0;


static NSString *cellid = @"cellBid";
// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, PanDirection) {
    PanDirectionHorizontalMoved, //横向移动
    PanDirectionVerticalMoved    //纵向移动
};

@interface LocalViewController () <UIGestureRecognizerDelegate, UIScrollViewDelegate> {
    CameraMoveDirection direction;
}
@property(nonatomic, strong) AVQueuePlayer *queuePlayer;
@property(nonatomic, strong) AVPlayerLayer *layer;
@property(nonatomic, strong) NSMutableArray<AVPlayerItem *> *playerItems;

/** 定义一个实例变量，保存枚举值 */
@property(nonatomic, assign) PanDirection panDirection;
@property(nonatomic, assign) int currentItem;
@property(nonatomic, assign) CGFloat currentTimeSeconds;//当前播放的秒数
@property(nonatomic, assign) CMTime cmTime;//当前播放的秒数

@property (nonatomic, strong) UIPanGestureRecognizer *pan; // 滑动手势

/**
 *  自己进行测试添加的一个播放器
 */
@property(nonatomic, strong) AVPlayerItemVideoOutput *videoOutput;
@property(nonatomic, strong) CIImage *image;

@property (nonatomic, strong) NSArray <NSURL *> *urlPath; // 视频的地址
@property (nonatomic, strong) NSMutableArray <UIImageView *>*imageViewArray; // 视频每帧截图的数组
@property (nonatomic, weak) UIScrollView *videoImageBackgroundView; // 切换视频的时候选择的背景图片

/**
 *  切换视频之间添加的图片
 */
@property(nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) dispatch_queue_t queue; // 加载视图的线程

@end

@implementation LocalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"首页......");

    //最老的一种
//    XSMediaPlayer *xsMediaPlayer = [[XSMediaPlayer alloc] initWithFrame:self.view.bounds];
//    xsMediaPlayer.videoURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]  pathForResource:@"video1" ofType:@"mp4"]];
//    [self.view addSubview:xsMediaPlayer];

    _videoOutput = [[AVPlayerItemVideoOutput alloc] initWithPixelBufferAttributes:nil];
    self.currentItem = 0;
    [self createAVQueuePlayer];
    [self addPanGesuture];
    [self setTheProgressOfPlayTime];

    [self.queuePlayer.currentItem addOutput:_videoOutput];


    /*  */

}

- (void)addPanGesuture {
    // 添加平移手势，用来控制音量、亮度、快进快退
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDirection:)];
    pan.delegate = self;
    _pan = pan;
    [self.view addGestureRecognizer:pan];
}

/**
 * 创建AVQueue播放器
 */
- (void)createAVQueuePlayer {
    //http://www.cocoachina.com/bbs/read.php?tid-308030-page-e-fpage-45.html

    NSString *path = @"";
    NSString *path2 = @"";
    NSString *path3 = @"";
    NSString *path4 = @"";

    NSURL *url = nil;
    NSURL *url2 = nil;
    NSURL *url3 = nil;
    NSURL *url4 = nil;


    if (self.isLocal) {
        path = [[NSBundle mainBundle] pathForResource:@"aa1" ofType:@"avi"];
        path2 = [[NSBundle mainBundle] pathForResource:@"aa2" ofType:@"avi"];
        path3 = [[NSBundle mainBundle] pathForResource:@"aa3" ofType:@"avi"];
        path4 = [[NSBundle mainBundle] pathForResource:@"video1" ofType:@"mp4"];

//        path  = [[NSBundle mainBundle]  pathForResource:@"aa1" ofType:@"avi"];
//        path2 = [[NSBundle mainBundle] pathForResource:@"aa2" ofType:@"avi"];
//        path3 = [[NSBundle mainBundle] pathForResource:@"aa3" ofType:@"avi"];
//        path4 = [[NSBundle mainBundle] pathForResource:@"aa1" ofType:@"avi"];

        url = [NSURL fileURLWithPath:path];
        url2 = [NSURL fileURLWithPath:path2];
        url3 = [NSURL fileURLWithPath:path3];
        url4 = [NSURL URLWithString:path4];
        self.urlPath = @[url, url2, url3, url4];
//        url4 = [NSURL fileURLWithPath:path4];
    } else {
//        path  = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466736960951.mp4";
//        path2 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466736862604.mp4";
//        path3 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466482852436.mp4";
//        path4 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466482733434.mp4";
//
//        url  =[NSURL URLWithString:path];
//        url2 =[NSURL URLWithString:path2];
//        url3 =[NSURL URLWithString:path3];
//        url4 =[NSURL URLWithString:path4];
        path = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466736960951.mp4";
        path2 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466736862604.mp4";
        path3 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466482852436.mp4";
        path4 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466482733434.mp4";

//        path  = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466736960951.mp4";
//        path2 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466736862604.mp4";
//        path3 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466482852436.mp4";
//        path4 = @"http://114.215.84.189:8888/liuda/upload/matchLive/1466482733434.mp4";
//
//        path = @"http://180.166.202.80:8080/smgvc/upload/b6dca0af-5f21-4947-b888-ba01848de203.mp4";
//        path2 =@"http://180.166.202.80:8080/smgvc/upload/f4c6273a-c98c-46da-9154-a33ca1df8aa8.mp4";
//        path3 = @"http://180.166.202.80:8080/smgvc/upload/971da1b6-e03b-40f4-aee8-848daf2f404b.mp4";
//        path4 = @"http://180.166.202.80:8080/smgvc/upload/2fde8939-4d66-4dbf-8c1e-87483e3ca990.mp4";

        url = [NSURL URLWithString:path];
        url2 = [NSURL URLWithString:path2];
        url3 = [NSURL URLWithString:path3];
        url4 = [NSURL URLWithString:path4];
        self.urlPath = @[url, url2, url3, url4];
    }

    AVPlayerItem *thePlayerItemA = [[AVPlayerItem alloc] initWithURL:url];
    AVPlayerItem *thePlayerItemB = [[AVPlayerItem alloc] initWithURL:url2];
    AVPlayerItem *thePlayerItemC = [[AVPlayerItem alloc] initWithURL:url3];
    AVPlayerItem *thePlayerItemD = [[AVPlayerItem alloc] initWithURL:url4];
    self.playerItems = [NSMutableArray array];
    [self.playerItems addObject:thePlayerItemA];
    [self.playerItems addObject:thePlayerItemB];
    [self.playerItems addObject:thePlayerItemC];
    [self.playerItems addObject:thePlayerItemD];

    NSArray *theItems = [NSArray arrayWithObjects:thePlayerItemA, thePlayerItemB, thePlayerItemC, thePlayerItemD, nil];
    self.queuePlayer = [AVQueuePlayer queuePlayerWithItems:theItems];

    self.layer = [AVPlayerLayer playerLayerWithPlayer:self.queuePlayer];
    self.layer.backgroundColor = [UIColor grayColor].CGColor;
    self.layer.frame = CGRectMake(0, 0, self.view.frame.size.width, 400);
    [self.view.layer addSublayer:self.layer];

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(playerItemDidReachEnd:)
//                                                 name:AVPlayerItemDidPlayToEndTimeNotification
//                                               object:[theItems lastObject]];
//    [queuePlayer replaceCurrentItemWithPlayerItem:[queuePlayer items][0]];

//    [queuePlayer advanceToNextItem];
    [self.queuePlayer play];

    [self.queuePlayer.currentItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];

//    [self performSelector:@selector(delay1) withObject:nil afterDelay:10];
//    [self performSelector:@selector(delay2) withObject:nil afterDelay:20];
//    [self performSelector:@selector(delay3) withObject:nil afterDelay:30];
//    [self performSelector:@selector(delay4) withObject:nil afterDelay:40];


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive:)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];

}

- (void)refreshDisplay:(CADisplayLink *)sender {
    CMTime itemTime = [_videoOutput itemTimeForHostTime:CACurrentMediaTime()];
    if ([_videoOutput hasNewPixelBufferForItemTime:itemTime]) {
        CVPixelBufferRef pixelBuffer = [_videoOutput copyPixelBufferForItemTime:itemTime itemTimeForDisplay:nil];
        _image = [CIImage imageWithCVPixelBuffer:pixelBuffer];
        // 利用 CIFilter 处理 CIImage
        CVPixelBufferRelease(pixelBuffer);
    }
}

- (void)playAtIndex:(NSInteger)index {

    [self.queuePlayer removeAllItems];
    NSLog(@"playAtIndex:%i", index);
    for (int i = index; i < self.playerItems.count; i++) {
        AVPlayerItem *obj = [self.playerItems objectAtIndex:i];
        if ([self.queuePlayer canInsertItem:obj afterItem:nil]) {

            //转换成CMTime才能给player来控制播放进度
            CGFloat seconds = self.currentTimeSeconds;
            CMTime cgtime = CMTimeMake((int) (seconds * 1000000) + 100, 1000000);

            //[obj seekToTime:kCMTimeZero];
            NSLog(@"-------------seconds:%lf    %lf", self.currentTimeSeconds, seconds);

            [self.queuePlayer insertItem:obj afterItem:nil];

            self.cmTime = CMTimeMake(self.cmTime.value + 10, self.cmTime.timescale);

            //            [obj seekToTime:cgtime];
//            [obj seekToTime:self.cmTime];
            @try {
                [obj seekToTime:CMTimeMakeWithSeconds(fabs(seconds), self.cmTime.timescale)
                toleranceBefore:kCMTimeZero
                 toleranceAfter:kCMTimeZero
              completionHandler:^(BOOL finished) {
                  [self.queuePlayer play];
              }];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception occurred: %@, %@", exception, [exception userInfo]);
            }


            [obj addOutput:_videoOutput];

            self.title = [NSString stringWithFormat:@"视频:%i", index];
            break;
        }

    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.imageView removeFromSuperview];
        self.imageView = nil;
    });

}


#pragma mark - 平移手势方法

- (void)panDirection:(UIPanGestureRecognizer *)pan {
//    //根据在view上Pan的位置，确定是调音量还是亮度
    CGPoint locationPoint;
//
    CGPoint translation = [pan translationInView:self.view];


    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [pan velocityInView:self.view];
    BOOL isLeft = YES;

    // 判断是垂直移动还是水平移动
    switch (pan.state) {
        case UIGestureRecognizerStateBegan: { // 开始移动
            locationPoint = [pan locationInView:self.view];;
            direction = kCameraMoveDirectionNone;

            // 使用绝对值来判断移动的方向
            CGFloat x = fabs(veloctyPoint.x);
            CGFloat y = fabs(veloctyPoint.y);
            if (x > y) { // 水平移动
                self.panDirection = PanDirectionHorizontalMoved;
//                self.isDragSlider = YES;
            } else if (x < y) { // 垂直移动
                self.panDirection = PanDirectionVerticalMoved;
            }
            break;
        }
        case UIGestureRecognizerStateChanged: { // 正在移动
            
            [self.queuePlayer pause];
            self.imageViewArray = [NSMutableArray array];
            CGFloat weight = [UIScreen mainScreen].bounds.size.width / self.urlPath.count;
            self.videoImageBackgroundView;
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            _queue = dispatch_queue_create("test", NULL);
//            dispatch_async(_queue, ^{
                [self.urlPath enumerateObjectsUsingBlock:^(NSURL * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    UIImageView *iamgeView = [[UIImageView alloc] initWithFrame:CGRectMake(idx * weight + [UIScreen mainScreen].bounds.size.width / 2, 0, weight, 100)];
                    CMTime time = [_queuePlayer.currentItem duration];
                    [self.imageViewArray addObject:iamgeView];
                    iamgeView.image = [LocalViewController thumbnailImageForVideo:obj atTime:self.currentTimeSeconds];
                    [self.videoImageBackgroundView addSubview:iamgeView];
                }];
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [se/UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//                });
//            });
            pan.enabled = NO;
            if (direction == kCameraMoveDirectionNone) {
                direction = [self determineCameraDirectionIfNeeded:translation];
            }
            switch (self.panDirection) {
                case PanDirectionHorizontalMoved: {
//                    NSLog(@"verticalMoved正在移动--水平移动--");
//                    [self horizontalMoved:veloctyPoint.x]; // 水平移动的方法只要x方向的值
//                    [self progressSliderValueChanged:self.maskView.videoSlider];
                    break;
                }
                case PanDirectionVerticalMoved: {
//                    if (locationPoint.x <= self.view.frame.size.width / 2.0f) {
//                        NSLog(@"verticalMoved正在移动--垂直移动--声音-");
////                        [self verticalMoved_volume:veloctyPoint.y]; // 垂直移动方法只要y方向的值
//                    } else {
//                        NSLog(@"verticalMoved正在移动--垂直移动--亮度-");
////                        [self verticalMoved_brightness:veloctyPoint.y];
//                    }
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case UIGestureRecognizerStateEnded: { // 移动停止
            [self.videoImageBackgroundView removeFromSuperview];
//            // 移动结束也需要判断垂直或者平移
//            // 比如水平移动结束时，要快进到指定位置，如果这里没有判断，当我们调节音量完之后，会出现屏幕跳动的bug
//            switch (self.panDirection) {
//                case PanDirectionHorizontalMoved: {
////                    [self progressSliderTouchEnded:self.maskView.videoSlider];
//                    break;
//                }
//                case PanDirectionVerticalMoved: {
//                    // 垂直移动结束后，把状态改为不再控制音量
//
//                    break;
//                }
//                default:
//                    break;
//            }
//            break;

            if (!CMTimeGetSeconds(self.cmTime) > 0.0) {
                return;
            }
            CMTime itemTime = [_videoOutput itemTimeForHostTime:CACurrentMediaTime()];
            if ([_videoOutput hasNewPixelBufferForItemTime:itemTime]) {
                CVPixelBufferRef pixelBuffer = [_videoOutput copyPixelBufferForItemTime:itemTime itemTimeForDisplay:nil];
                // 利用 CIFilter 处理 CIImage
                _imageView = [[UIImageView alloc] initWithImage:[UIImage imageWithCIImage:[CIImage imageWithCVPixelBuffer:pixelBuffer]]];
                _imageView.frame = self.layer.videoRect;
                [self.view addSubview:_imageView];

                CVPixelBufferRelease(pixelBuffer);
            }

            if (direction == kCameraMoveDirectionLeft) {
                NSLog(@"左侧");
                self.currentItem++;
                self.currentItem = (self.currentItem >= (self.playerItems.count) ? 0 : self.currentItem);
                [self playAtIndex:self.currentItem];
//                [CAAnimationHelp addAnimalUseCATrasition:self.view withType:CATrasitionType_pageCurl];
            } else if (direction == kCameraMoveDirectionRight) {
                NSLog(@"右侧");
                self.currentItem--;
                self.currentItem = self.currentItem < 0 ? self.playerItems.count-1 : self.currentItem;
                [self playAtIndex:self.currentItem];
//                [CAAnimationHelp addAnimalUseCATrasition:self.view withType:CATrasitionType_pageUnCurl];
            } else {
                NSLog(@"其他");
            }

        }
        default:
            break;
    }
}

//设置播放进度和时间
- (void)setTheProgressOfPlayTime {

    [self.queuePlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 100) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        AVPlayerItem *playerItem = self.queuePlayer.currentItem;
        int timescale = playerItem.duration.timescale;

        float current = CMTimeGetSeconds(time);
        self.currentTimeSeconds = current;
        self.cmTime = time;

//        NSLog(@"%lf", current);

//        float total=CMTimeGetSeconds([playerItem duration]);

//        //秒数
//        NSInteger proSec = (NSInteger) current % 60;
//        //分钟
//        NSInteger proMin = (NSInteger) current / 60;
//
//        //总秒数和分钟
//        NSInteger durSec = (NSInteger) total % 60;
//        NSInteger durMin = (NSInteger) total / 60;
//        NSString *totalStr = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];

//        NSLog(@"当前已经播放%.2fs;  总秒数:%li; 帧率:%i;  总S:%@",  current, current/total,  timescale, totalStr);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.queuePlayer pause];
    self.queuePlayer = nil;
}

- (void)dealloc {
    NSLog(@"dealloc--");
    [self.queuePlayer pause];
    self.queuePlayer = nil;
}

- (void)viewWillLayoutSubviews NS_AVAILABLE_IOS(5_0) {
    [super viewWillLayoutSubviews];
    NSLog(@"viewWillLayoutSubviews--");
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *delegage = [[UIApplication sharedApplication] delegate];
    [_videoImageBackgroundView removeFromSuperview];
    UIButton *button = [delegage.window viewWithTag:100];
    UIView *view = [delegage.window viewWithTag:200];
    [button removeFromSuperview];
    [view removeFromSuperview];
    if (_queue) {
        dispatch_suspend(_queue);
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    NSLog(@"viewDidLayoutSubviews--");
    self.view.backgroundColor = [UIColor clearColor];
    self.layer.frame = self.view.bounds;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *, id> *)change context:(void *)context {
    AVPlayerItem *playerItem = object;
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerStatus status = [[change objectForKey:@"new"] intValue];
        if (status == AVPlayerStatusReadyToPlay) {
            NSLog(@"正在播放...，视频总长度:%.2f", CMTimeGetSeconds(playerItem.duration));
        }
    } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
        NSArray *array = playerItem.loadedTimeRanges;
        CMTimeRange timeRange = [array.firstObject CMTimeRangeValue];//本次缓冲时间范围
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        NSTimeInterval totalBuffer = startSeconds + durationSeconds;//缓冲总长度
        NSLog(@"共缓冲：%.2f", totalBuffer);
    }
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    [self.queuePlayer pause];
}

- (void)applicationDidBecomeActive:(NSNotification *)notification {
    [self.queuePlayer play];
}


//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
//{
//    AVPlayerItem *playerItem=object;
//    if ([keyPath isEqualToString:@"status"]) {
//        AVPlayerStatus status= [[change objectForKey:@"new"] intValue];
//        if(status==AVPlayerStatusReadyToPlay){
//            NSLog(@"正在播放...，视频总长度:%.2f",CMTimeGetSeconds(playerItem.duration));
//        }
//    }
//    else if([keyPath isEqualToString:@"loadedTimeRanges"])
//    {
//        NSArray *array=playerItem.loadedTimeRanges;
//        CMTimeRange timeRange = [array.firstObject CMTimeRangeValue];//本次缓冲时间范围
//        float startSeconds = CMTimeGetSeconds(timeRange.start);
//        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
//        NSTimeInterval totalBuffer = startSeconds + durationSeconds;//缓冲总长度
//        NSLog(@"共缓冲==============：%.2f",totalBuffer);
//    }
//}


// This method will determine whether the direction of the user's swipe

- (CameraMoveDirection)determineCameraDirectionIfNeeded:(CGPoint)translation {

    if (direction != kCameraMoveDirectionNone)
        return direction;
    // determine if horizontal swipe only if you meet some minimum velocity

    BOOL gestureHorizontal = NO;

    if (translation.y == 0.0)
        gestureHorizontal = YES;
    else
        gestureHorizontal = (fabs(translation.x / translation.y) > 5.0);

    if (gestureHorizontal) {

        if (translation.x > 0.0)
            return kCameraMoveDirectionRight;
        else
            return kCameraMoveDirectionLeft;

    }
    return direction;

}

- (UIScrollView *)videoImageBackgroundView {
    if (!_videoImageBackgroundView) {
        UIScrollView *view = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 150, [UIScreen mainScreen].bounds.size.width, 100)];
        AppDelegate *delegage = [[UIApplication sharedApplication] delegate];
        [delegage.window addSubview:view];
        _videoImageBackgroundView = view;
        _videoImageBackgroundView.backgroundColor = [UIColor whiteColor];
        _videoImageBackgroundView.delegate = self;
        _videoImageBackgroundView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * 2, 100);
        _videoImageBackgroundView.showsHorizontalScrollIndicator = NO;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(scrollerViewPlay:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake([UIScreen mainScreen].bounds.size.width / 2 - 25, [UIScreen mainScreen].bounds.size.height - 50, 50, 50);
        [button setTitle:@"播放" forState:UIControlStateNormal];
        [delegage.window addSubview:button];
        button.tag = 100;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height - 150, 2, 105)];
        lineView.tag = 200;
        [delegage.window addSubview:lineView];
        lineView.backgroundColor = [UIColor blueColor];
    }
    return _videoImageBackgroundView;
                                                                
}

- (void)scrollerViewPlay:(UIButton *)sender {
    AppDelegate *delegage = [[UIApplication sharedApplication] delegate];
    [_videoImageBackgroundView removeFromSuperview];
    UIButton *button = [delegage.window viewWithTag:100];
    UIView *view = [delegage.window viewWithTag:200];
    [button removeFromSuperview];
    [view removeFromSuperview];
    _pan.enabled = YES;

     __block NSInteger idex = 0;
    __block CGFloat x;
    [self.imageViewArray enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        idex = idx;
        x = obj.frame.origin.x - _videoImageBackgroundView.contentOffset.x;
        if ([UIScreen mainScreen].bounds.size.width / 2 >= x && [UIScreen mainScreen].bounds.size.width / 2 <= x + [UIScreen mainScreen].bounds.size.width / 4) {
            *stop = YES;
        }
    }];

    
    self.currentItem = idex;
    [self playAtIndex:self.currentItem];
    [self.queuePlayer play];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

+ (UIImage*) thumbnailImageForVideo:(NSURL *)videoURL atTime:(NSTimeInterval)time {
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    NSParameterAssert(asset);
    AVAssetImageGenerator *assetImageGenerator =[[AVAssetImageGenerator alloc] initWithAsset:asset];
    assetImageGenerator.appliesPreferredTrackTransform = YES;
    assetImageGenerator.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;
    
    CGImageRef thumbnailImageRef = NULL;
    CFTimeInterval thumbnailImageTime = time;
    NSError *thumbnailImageGenerationError = nil;
    thumbnailImageRef = [assetImageGenerator copyCGImageAtTime:CMTimeMake(thumbnailImageTime, 1)actualTime:NULL error:&thumbnailImageGenerationError];
    
    if(!thumbnailImageRef)
        NSLog(@"thumbnailImageGenerationError %@",thumbnailImageGenerationError);
    
    UIImage*thumbnailImage = thumbnailImageRef ? [[UIImage alloc]initWithCGImage: thumbnailImageRef] : nil;
    
    return thumbnailImage;
}

@end
