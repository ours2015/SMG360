//
//  QMAVplayerVC.m
//  TongYan
//
//  Created by 段大志 on 16/6/15.
//  Copyright © 2016年 Alone. All rights reserved.
//

#import "QMAVplayerVC.h"
#import "XSMediaPlayer.h"
#import "LocalViewController.h"

@interface QMAVplayerVC ()
@property (strong, nonatomic) XSMediaPlayer *xsmediaPlayer;
@end

@implementation QMAVplayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.view.backgroundColor = [UIColor blackColor];
    self.xsmediaPlayer = [[XSMediaPlayer alloc]initWithFrame:CGRectMake(0, 0, UIScreenWidth, UIScreenWidth*0.75)];
    self.xsmediaPlayer.center = CGPointMake(UIScreenWidth/2.0F, UIScreenHeight/2.0F);
    self.xsmediaPlayer.backgroundColor = [UIColor blackColor];
    __weak QMAVplayerVC *weakSelf = self;
    self.xsmediaPlayer.vc = weakSelf;

    //测试
    //    _player.videoURL = [NSURL fileURLWithPath:path];
//    self.xsmediaPlayer.videoURL = [NSURL URLWithString:@"http://114.215.84.189:8080/smgvc/upload/b359d6a5-4d66-405a-a9a3-2441547803b5.mp4"];

    NSLog(@"%@", self.videoUrl);
    self.xsmediaPlayer.videoURL = [NSURL URLWithString:self.videoUrl];
    [self.view addSubview:self.xsmediaPlayer];


    //屏蔽默认事件
    self.xsmediaPlayer.userInteractionEnabled = YES;
    self.xsmediaPlayer.multipleTouchEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
    [self.xsmediaPlayer addGestureRecognizer:tap];

    self.view.userInteractionEnabled = YES;
    self.view.multipleTouchEnabled   = YES;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)]];

    AppDelegate  *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.allowRotation = YES;
}

- (void) dismiss:(UITapGestureRecognizer *)tapGestureRecognizer {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    [self.xsmediaPlayer destoryAVplayer];
    AppDelegate  *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.allowRotation = NO;
    NSLog(@"--dismiss--");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait) {
        NSLog(@"横屏 - UIInterfaceOrientationPortrait---");
        [UIApplication sharedApplication].statusBarHidden = NO;
    }else if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"竖屏 - UIInterfaceOrientationLandscapeRight---");
        [UIApplication sharedApplication].statusBarHidden = NO;
    }
}
// 哪些页面支持自动转屏
- (BOOL)shouldAutorotate{
    return YES;
}


- (void) dealloc {
    NSLog(@"--dealloc----");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
