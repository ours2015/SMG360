//
//  XSMediaPlayer.h
//  MovieDemo
//
//  Created by zhao on 16/3/25.
//  Copyright © 2016年 Mega. All rights reserved.
//

#import "AppDelegate.h"

//http://code.cocoachina.com/view/130394  demo见
//简单的视频:AVplayer,AvplayerLayer,AVplayerItem
//AVQueuePlayer :是AVplayer的子类
//多视频-联合播放-http://www.cnblogs.com/isItOk/p/4875713.html

@interface XSMediaPlayer : UIView
/** 视频URL */
@property (nonatomic, strong) NSURL   *videoURL;
@property (nonatomic, weak) UIViewController *vc;


@property(assign, nonatomic) long long currentTime;//视频当前播放的时间

#pragma mark --  供外部调用,让视频 从 一个时间节点开始 ----
-(void) progressSliderEnd:(NSString *)dragedSecondsStr;
-(void) destoryAVplayer;

@end



