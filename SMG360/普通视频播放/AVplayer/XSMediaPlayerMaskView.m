//
//  XSMediaPlayerMaskView.m
//  MovieDemo
//
//  Created by zhao on 16/3/25.
//  Copyright © 2016年 Mega. All rights reserved.
//

#import "XSMediaPlayerMaskView.h"
#import "LocalViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "UIView+Extension.h"
#import "UIColor+Extension.h"

@interface XSMediaPlayerMaskView () {
    UILabel*_titleLable;
}


@end

@implementation XSMediaPlayerMaskView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        self.topImageView = [[UIView alloc]init];
        self.topImageView.backgroundColor = [UIColor blueColor];
        self.bottomImageView = [[UIImageView alloc]init];
//        self.bottomImageView.backgroundColor = [UIColor redColor];
        self.bottomImageView.userInteractionEnabled = YES;
        
        self.startBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,50,50)];
        [self.startBtn setImage:[UIImage imageNamed:@"kr-video-player-play"] forState:UIControlStateNormal];
        [self.startBtn setImage:[UIImage imageNamed:@"kr-video-player-pause"] forState:UIControlStateSelected];
//        self.startBtn.backgroundColor = [UIColor redColor];
        
        self.startBigBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,80,80)];
        [self.startBigBtn setImage:[UIImage imageNamed:@"kr-video-player-play"] forState:UIControlStateNormal];
        self.startBigBtn.hidden = YES;
    

        
        self.fullScreenBtn = [[UIButton alloc]init];
//        self.fullScreenBtn.backgroundColor = [UIColor redColor];
        [self.fullScreenBtn setImage:[UIImage imageNamed:@"kr-video-player-fullscreen"] forState:UIControlStateNormal];
         [self.fullScreenBtn setImage:[UIImage imageNamed:@"kr-video-player-fullscreen"] forState:UIControlStateSelected];
        
        self.currentTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 10,60, 30)];
        self.currentTimeLabel.text = @"00:00";
        self.currentTimeLabel.textColor = [UIColor whiteColor];
        self.currentTimeLabel.textAlignment = NSTextAlignmentCenter;
        self.currentTimeLabel.font = [UIFont systemFontOfSize:15];
        self.totalTimeLabel = [[UILabel alloc]init];
        self.totalTimeLabel.textAlignment = NSTextAlignmentCenter;
        self.totalTimeLabel.font = [UIFont systemFontOfSize:15];
        self.totalTimeLabel.textColor = [UIColor whiteColor];
        self.totalTimeLabel.text = @"00:00";
        
    
        self.progressView = [[UIProgressView alloc]init];
        self.progressView.progressTintColor    = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.3];
        self.progressView.trackTintColor       = [UIColor clearColor];
        
        self.volumeProgress = [[UIProgressView alloc]init];
        self.volumeProgress.transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.volumeProgress.hidden = YES;
        self.volumeProgress.backgroundColor      = [UIColor orangeColor];
        self.volumeProgress.progressTintColor    = [UIColor redColor];
        self.volumeProgress.trackTintColor       = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.3];
        

        // 设置slider
        self.videoSlider = [[UISlider alloc]init];
        [self.videoSlider setThumbImage:[UIImage imageNamed:@"slider"] forState:UIControlStateNormal];
        self.videoSlider.minimumTrackTintColor = [UIColor hexValue:0x000000];
        self.videoSlider.maximumTrackTintColor = [UIColor hexValue:0xB5B5B5];
        
        [self addSubview:self.topImageView];
        [self addSubview:self.bottomImageView];
        // 初始化渐变层
        [self initCAGradientLayer];

        
        self.activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self.bottomImageView addSubview:self.startBtn];
        [self.bottomImageView addSubview:self.fullScreenBtn];
        [self.bottomImageView addSubview:self.currentTimeLabel];
        [self.bottomImageView addSubview:self.totalTimeLabel];
        [self.bottomImageView addSubview:self.progressView];
        [self.bottomImageView addSubview:self.videoSlider];
        [self addSubview:self.volumeProgress];
        
        [self addSubview:self.activity];
      
    
        //添加顶部的所有自己添加组件
        [self createTopView];
        
        [self addSubview:self.startBigBtn];

        NSError *error;
        
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        // add event handler, for this example, it is `volumeChange:` method
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
    }
    return self;
}


- (void)volumeChanged:(NSNotification *)notification
{
    // service logic here.
    NSLog(@"系统声音被改变:%@",notification.userInfo);
    NSString *valueStr = notification.userInfo[@"AVSystemController_AudioVolumeNotificationParameter"];
    self.volumeProgress.progress = [valueStr floatValue];
    
}



-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    self.topImageView.frame = CGRectMake(0, 0,width, 50);
    self.bottomImageView.frame = CGRectMake(0,height-50, width, 50);
    self.bottomGradientLayer.frame = self.bottomImageView.bounds;
    self.topGradientLayer.frame    = self.topImageView.bounds;
    self.startBigBtn.center  = self.center;
    
    self.fullScreenBtn.frame = CGRectMake(width-50,0,50,50);

    CGFloat progressWidth = width-2*(self.startBtn.frame.size.width+self.currentTimeLabel.frame.size.width);
    if (self.vc.view.width > self.vc.view.height) {//横屏
        progressWidth -= 50;
    }

    
    self.progressView.frame = CGRectMake(0,0,progressWidth,20);
    self.progressView.center = CGPointMake(width/2, 25);
    self.videoSlider.frame = self.progressView.frame;
    self.activity.center = CGPointMake(width/2, height/2);
    self.volumeProgress.bounds = CGRectMake(0, 0,100,30);
    self.volumeProgress.center = CGPointMake(40,height/2);
    self.totalTimeLabel.frame = CGRectMake(width-110,10,60,30);
    if (self.vc.view.width > self.vc.view.height) {//横屏
           self.totalTimeLabel.frame = CGRectMake(width-110-30,10,60,30);
    }


    CGFloat TopViewHeight = 44;
    if (self.vc.view.width > self.vc.view.height) {//横屏
       self.backBtn.hidden = NO;
        _titleLable.hidden = NO;
    } else {
        self.backBtn.hidden = YES;
        _titleLable.hidden  = YES;
    }
    _titleLable.width = self.vc.view.width - self.backBtn.right - 10;

}

- (void)initCAGradientLayer
{

    //初始Top化渐变层
//    self.topGradientLayer               = [CAGradientLayer layer];
//    [self.topImageView.layer addSublayer:self.topGradientLayer];
//    //设置渐变颜色方向
//    self.topGradientLayer.startPoint    = CGPointMake(0, 0);
//    self.topGradientLayer.endPoint      = CGPointMake(0, 1);
//    //设定颜色组
//    self.topGradientLayer.colors        = @[ (__bridge id)[UIColor lightGrayColor].CGColor,
//            (__bridge id)[UIColor clearColor].CGColor];
//    //设定颜色分割点
//    self.topGradientLayer.locations     = @[@(0.0f) ,@(1.0f)];
    self.topImageView.backgroundColor = [UIColor hexValue:0x000000 alpha:0.5];


    //初始化Bottom渐变层
    self.bottomGradientLayer            = [CAGradientLayer layer];
    [self.bottomImageView.layer addSublayer:self.bottomGradientLayer];
    //设置渐变颜色方向
    self.bottomGradientLayer.startPoint = CGPointMake(0, 0);
    self.bottomGradientLayer.endPoint   = CGPointMake(0, 1);
    //设定颜色组
    self.bottomGradientLayer.colors     = @[(__bridge id)[UIColor clearColor].CGColor,
                                            (__bridge id)[UIColor lightGrayColor].CGColor];
    //设定颜色分割点
    self.bottomGradientLayer.locations  = @[@(0.0f) ,@(1.0f)];
}


#pragma mark 顶部自定义的 控件
- (void)createTopView{

    CGFloat TopViewHeight = 33;

    UIImage *returnimage  = [UIImage imageNamed:@"arrow"];

    self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backBtn.backgroundColor = [UIColor clearColor];
    self.backBtn.contentMode = UIViewContentModeCenter;
    [self.backBtn setImage:[UIImage imageNamed:@"arrow"] forState:UIControlStateNormal];
    [self.backBtn setFrame:CGRectMake(0, 0, 50, 70)];
    [self.topImageView addSubview:self.backBtn];

    _titleLable = [[UILabel alloc]initWithFrame:CGRectMake(returnimage.size.width+30, 18 , UIScreenHeight - self.backBtn.right - 10, TopViewHeight)];
    _titleLable.backgroundColor = [UIColor clearColor];
    _titleLable.text = @"视频";
    _titleLable.textColor = [UIColor whiteColor];
    _titleLable.textAlignment = NSTextAlignmentLeft;
    _titleLable.font = [UIFont systemFontOfSize:15];
    [self.topImageView addSubview:_titleLable];
}


-(void)dealloc
{
    NSLog(@"XSMediaPlayerMaskView -- dealloc %s",__func__);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
}

@end
