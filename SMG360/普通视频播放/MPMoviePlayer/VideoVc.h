//
//  VideoVc.h
//  TongYan
//
//  Created by 段大志 on 16/4/21.
//  Copyright © 2016年 Alone. All rights reserved.
//


@class JSONModel;
@class MatchLiveVo;


#import <UIKit/UIKit.h>


#define UIScreenWidth  [UIScreen mainScreen].bounds.size.width
#define UIScreenHeight [UIScreen mainScreen].bounds.size.height

// 视频播放页面,注意,要跳转到这个页面的ViewController 要在获取焦点的是设置 navigationBar 不隐藏
//1:直接全屏,如何做
//2:播放按钮控制
//3:默认图片
//http://www.jianshu.com/p/d2234601dd07 简单介绍
//关于MPMoviePlayerController禁用缩放和点击 http://ju.outofmemory.cn/entry/11436

@interface VideoVc : UIViewController
@property (nonatomic,strong)NSString *videoUrl;
@end
