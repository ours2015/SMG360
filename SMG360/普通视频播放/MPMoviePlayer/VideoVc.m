//
//  VideoVc.m
//  TongYan
//
//  Created by 段大志 on 16/4/21.
//  Copyright © 2016年 Alone. All rights reserved.
//


#import <UIKit/UIKit.h>

#define UIScreenWidth  [UIScreen mainScreen].bounds.size.width
#define UIScreenHeight [UIScreen mainScreen].bounds.size.height

#import "VideoVc.h"
#import <MediaPlayer/MediaPlayer.h>
#import "AppDelegate.h"

@interface VideoVc ()

@property (weak, nonatomic) IBOutlet UIView *playView;
@property(nonatomic,strong) MPMoviePlayerController *moviePlayer;
@property(nonatomic,strong) UIImageView *defaultIv;//默认的图片

@end

@implementation VideoVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"视频";
    [self startPlayer];
    //进入全屏
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerWillEnterFullscreenNotification:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:nil];
    //退出全屏
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerWillExitFullscreenNotification:)
                                                 name:MPMoviePlayerWillExitFullscreenNotification
                                               object:nil];

    //退出全屏
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];

    self.view.userInteractionEnabled = YES;
    self.view.multipleTouchEnabled   = YES;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)]];
    //默认变成全屏
}


- (void) dismiss:(UITapGestureRecognizer *)tapGestureRecognizer {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"--dismiss--");
    [self stopPlayer];
}

- (void) dealloc {
    NSLog(@"--dealloc----");
    [self stopPlayer];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //有的程序开着的时候屏幕不灭是怎么做的？http://www.cocoachina.com/bbs/read.php?tid=93988
    UIApplication *app = [UIApplication sharedApplication];
    app.idleTimerDisabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    UIApplication *app = [UIApplication sharedApplication];
    app.idleTimerDisabled = NO;
}


-(MPMoviePlayerController *)moviePlayer{
    if (!_moviePlayer) {
        self.moviePlayer=[[MPMoviePlayerController alloc]init];
        self.moviePlayer.controlStyle = MPMovieControlStyleDefault;
        self.moviePlayer.view.frame = CGRectMake(0, 0, UIScreenWidth, UIScreenWidth * 0.74);;
        [_playView addSubview:self.moviePlayer.view];
        //设置全屏
        [self.moviePlayer setFullscreen:YES animated:YES];
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
        self.moviePlayer.repeatMode  = MPMovieRepeatModeNone;
        [self.moviePlayer prepareToPlay];
        self.moviePlayer.shouldAutoplay = YES;


        //屏蔽默认事件
        self.moviePlayer.view.userInteractionEnabled = YES;
        self.moviePlayer.view.multipleTouchEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
        [self.moviePlayer.view addGestureRecognizer:tap];
    }
    return _moviePlayer;
}

// 是否支持屏幕旋转
- (BOOL)shouldAutorotate {
    return YES;
}
// 支持的旋转方向
- (NSUInteger)supportedInterfaceOrientations {
    AppDelegate  *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    if (delegate.allowRotation) {
        return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
    }else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

#pragma mark - video-play
- (void)moviePlayerWillEnterFullscreenNotification:(NSNotification *)notification
{
    NSLog(@"全屏播放...");
    AppDelegate  *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.allowRotation = YES;

    self.defaultIv.hidden = YES;
    [self hideControls];
}


- (void)moviePlayerWillExitFullscreenNotification:(NSNotification *)notification
{
    NSLog(@"退出播放...");
    AppDelegate  *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.allowRotation = NO;

    self.defaultIv.hidden = NO;
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    //DLOG(@"上一曲,下一曲..");
    MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter]
            removeObserver:self
                      name:MPMoviePlayerPlaybackDidFinishNotification
                    object:player];

    if ([player
            respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark  播放控制
- (void) stopPlayer {
    [self.moviePlayer pause];
    [self.moviePlayer stop];
    self.moviePlayer  = nil;
}

- (void) startPlayer {
    //NSAssert(self.videoModel, @"startPlayer -- self.videoModel 是 nil ");
    //NSString *urStr = @"http://114.215.84.189:9001/cateringMaster/upload/disk/1460364598987.mp4";
    NSString *urStr = _videoUrl;
     ([NSString stringWithFormat:@"视频播放地址:%@", urStr]);
    NSString* encodedString = [urStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.moviePlayer.contentURL = [NSURL URLWithString:encodedString];
    [self.moviePlayer play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)hideControls
{
    for(id views in [[self.moviePlayer view] subviews]){
        for(id subViews in [views subviews]){
            for (id controlView in [subViews subviews]){
                if ( [controlView isKindOfClass:NSClassFromString(@"MPKnockoutButton")] ) {
                    UIButton *btn = controlView;
                    [btn setAlpha:0.0];
                    [btn setHidden:YES];
                    btn.frame = CGRectZero;
                }
            }
        }
    }
}

@end
